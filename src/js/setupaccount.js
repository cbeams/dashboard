'use strict';

/* global angular, $, showdown */

// create main application module
var app = angular.module('Application', ['pascalprecht.translate', 'ngCookies']);

app.filter('markdown2html', function () {
    var converter = new showdown.Converter({
        simplifiedAutoLink: true,
        strikethrough: true,
        tables: true,
        openLinksInNewWindow: true
    });

    return function (text) {
        return converter.makeHtml(text);
    };
});

// disable sce for footer https://code.angularjs.org/1.5.8/docs/api/ng/service/$sce
app.config(function ($sceProvider) {
    $sceProvider.enabled(false);
});

app.config(['$translateProvider', function ($translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: 'translation/',
        suffix: '.json'
    });
    $translateProvider.useLocalStorage();
    $translateProvider.preferredLanguage('en');
    $translateProvider.fallbackLanguage('en');
}]);

// Add shorthand "tr" filter to avoid having ot use "translate"
// This is a copy of the code at https://github.com/angular-translate/angular-translate/blob/master/src/filter/translate.js
// If we find out how to get that function handle somehow dynamically we can use that, otherwise the copy is required
function translateFilterFactory($parse, $translate) {

  'use strict';

  var translateFilter = function (translationId, interpolateParams, interpolation, forceLanguage) {
    if (!angular.isObject(interpolateParams)) {
      var ctx = this || {
        '__SCOPE_IS_NOT_AVAILABLE': 'More info at https://github.com/angular/angular.js/commit/8863b9d04c722b278fa93c5d66ad1e578ad6eb1f'
        };
      interpolateParams = $parse(interpolateParams)(ctx);
    }

    return $translate.instant(translationId, interpolateParams, interpolation, forceLanguage);
  };

  if ($translate.statefulFilter()) {
    translateFilter.$stateful = true;
  }

  return translateFilter;
}
translateFilterFactory.displayName = 'translateFilterFactory';
app.filter('tr', translateFilterFactory);

app.controller('SetupAccountController', ['$scope', '$translate', '$http', function ($scope, $translate, $http) {
    // Stupid angular location provider either wants html5 location mode or not, do the query parsing on my own
    var search = decodeURIComponent(window.location.search).slice(1).split('&').map(function (item) { return item.indexOf('=') === -1 ? [item, true] : [item.slice(0, item.indexOf('=')), item.slice(item.indexOf('=')+1)]; }).reduce(function (o, k) { o[k[0]] = k[1]; return o; }, {});

    var API_ORIGIN = '<%= apiOrigin %>' || window.location.origin;

    $scope.initialized = false;
    $scope.busy = false;
    $scope.error = null;
    $scope.view = 'setup';
    $scope.status = null;

    $scope.profileLocked = !!search.profileLocked;
    $scope.existingUsername = !!search.username;
    $scope.username = search.username || '';
    $scope.displayName = search.displayName || '';
    $scope.password = '';
    $scope.passwordRepeat = '';

    $scope.onSubmit = function () {
        $scope.busy = true;
        $scope.error = null;

        var data = {
            resetToken: search.resetToken,
            password: $scope.password
        };

        if (!$scope.profileLocked) {
            data.username = $scope.username;
            data.displayName = $scope.displayName;
        }

        function error(data, status) {
            $scope.busy = false;

            if (status === 401) {
                $scope.view = 'invalidToken';
            } else if (status === 409) {
                $scope.error = {
                    username: true,
                    message: 'Username already taken'
                };
                $scope.setupAccountForm.username.$setPristine();
                setTimeout(function () { $('#inputUsername').focus(); }, 200);
            } else if (status === 400) {
                $scope.error = {
                    message: data.message
                };
                if (data.message.indexOf('Username') === 0) {
                    $scope.setupAccountForm.username.$setPristine();
                    $scope.error.username = true;
                }
            } else {
                $scope.error = { message: 'Unknown error. Please try again later.' };
                console.error(status, data);
            }
        }

        $http.post(API_ORIGIN + '/api/v1/cloudron/setup_account', data).success(function (data, status) {
            if (status !== 201) return error(data, status);

            // set token to autologin
            localStorage.token = data.accessToken;

            $scope.view = 'done';
        }).error(error);
    };

    $http.get(API_ORIGIN + '/api/v1/cloudron/status').success(function (data, status) {
        $scope.initialized = true;

        if (status !== 200) return;

        if (data.language) $translate.use(data.language);

        $scope.status = data;
    }).error(function () {
        $scope.initialized = false;
    });
}]);
