'use strict';

/* global angular */
/* global $ */

angular.module('Application').controller('VolumesController', ['$scope', '$location', '$timeout', 'Client', function ($scope, $location, $timeout, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastUserManager) $location.path('/'); });

    $scope.config = Client.getConfig();
    $scope.volumes = [];
    $scope.ready = false;

    function refreshVolumes(callback) {
        Client.getVolumes(function (error, results) {
            if (error) return console.error(error);

            $scope.volumes = results;
            if (callback) callback();
        });
    }

    $scope.volumeAdd = {
        error: null,
        busy: false,

        name: '',
        hostPath: '',


        reset: function () {
            $scope.volumeAdd.busy = false;
            $scope.volumeAdd.error = null;
            $scope.volumeAdd.name = '';
            $scope.volumeAdd.hostPath = '';

            $scope.volumeAddForm.$setPristine();
            $scope.volumeAddForm.$setUntouched();
        },

        show: function () {
            $scope.volumeAdd.reset();

            $('#volumeAddModal').modal('show');
        },

        submit: function () {
            $scope.volumeAdd.busy = true;
            $scope.volumeAdd.error = null;

            Client.addVolume($scope.volumeAdd.name, $scope.volumeAdd.hostPath, function (error) {
                $scope.volumeAdd.busy = false;
                if (error) {
                    $scope.volumeAdd.error = error.message;
                    return;
                }

                $('#volumeAddModal').modal('hide');
                $scope.volumeAdd.reset();

                refreshVolumes();
            });
        }
    };

    $scope.volumeRemove = {
        busy: false,
        error: null,
        volume: null,

        reset: function () {
            $scope.volumeRemove.busy = false;
            $scope.volumeRemove.error = null;
            $scope.volumeRemove.volume = null;
        },

        show: function (volume) {
            $scope.volumeRemove.reset();

            $scope.volumeRemove.volume = volume;

            $('#volumeRemoveModal').modal('show');
        },

        submit: function () {
            $scope.volumeRemove.busy = true;
            $scope.volumeRemove.error = null;

            Client.removeVolume($scope.volumeRemove.volume.id, function (error) {
                if (error && (error.statusCode === 403 || error.statusCode === 409)) {
                    $scope.volumeRemove.error = error.message;
                } else if (error) {
                    Client.error(error);
                } else {
                    $('#volumeRemoveModal').modal('hide');
                    $scope.volumeRemove.reset();

                    refreshVolumes();
                }

                $scope.volumeRemove.busy = false;
            });
        },
    };

    Client.onReady(function () {
        refreshVolumes(function (error) {
            if (error) return console.error(error);

            $scope.ready = true;
        });
    });

    // setup all the dialog focus handling
    ['volumeAddModal', 'volumeRemoveModal'].forEach(function (id) {
        $('#' + id).on('shown.bs.modal', function () {
            $(this).find('[autofocus]:first').focus();
        });
    });

    $('.modal-backdrop').remove();
}]);
