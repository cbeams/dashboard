'use strict';

/* global async */
/* global angular */

angular.module('Application').controller('NotificationsController', ['$scope', '$timeout', 'Client', function ($scope, $timeout, Client) {
    $scope.clearAllBusy = false;

    $scope.notifications = {
        notifications: [],
        activeNotification: null,
        busy: true,

        refresh: function () {
            Client.getNotifications(false, 1, 100, function (error, result) {
                if (error) return console.error(error);

                // collapse by default
                result.forEach(function (r) { r.isCollapsed = true; });

                // attempt to parse the message as json
                result.forEach(function (r) {
                    try {
                        r.messageJson = JSON.parse(r.message);
                    } catch (e) {}
                });

                $scope.notifications.notifications = result;

                $scope.notifications.busy = false;
            });
        },

        clicked: function (notification) {
            if ($scope.notifications.activeNotification === notification) return $scope.notifications.activeNotification = null;
            $scope.notifications.activeNotification = notification;
        },

        ackOne: function (id, callback) {
            Client.ackNotification(id, function (error) {
                if (error) return callback(error);

                $scope.$parent.notificationAcknowledged(id);

                callback();
            });
        },

        ack: function (notification, event) {
            event.stopPropagation();

            $scope.notifications.ackOne(notification.id, function (error) {
                if (error) console.error(error);

                $scope.notifications.refresh();
            });
        },

        action: function (notification) {
            if (notification.action) window.location = notification.action;
        },

        clearAll: function () {
            $scope.clearAllBusy = true;

            async.eachLimit($scope.notifications.notifications, 20, function (notification, callback) {
                if (notification.acknowledged) return callback();
                $scope.notifications.ackOne(notification.id, callback);
            }, function (error) {
                if (error) console.error(error);

                $scope.notifications.refresh();
                $scope.clearAllBusy = false;
            });
        }
    };

    $scope.notificationExpanding = function (notification) {
        if (!notification.eventId) return;

        notification.busyLoadEvent = true;

        Client.getEvent(notification.eventId, function (error, result) {
            notification.busyLoadEvent = false;

            if (error) return console.error(error);

            notification.event = result;
        });
    };

    Client.onReady(function () {
        $scope.notifications.refresh();
    });

    Client.onReconnect(function () {
        $scope.notifications.refresh();
    });
}]);
