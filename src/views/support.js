'use strict';

/* global angular:false */
/* global $:false */

angular.module('Application').controller('SupportController', ['$scope', '$location', 'Client', function ($scope, $location, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastAdmin) $location.path('/'); });

    $scope.config = Client.getConfig();
    $scope.user = Client.getUserInfo();
    $scope.apps = Client.getInstalledApps();
    $scope.supportConfig = null;

    $scope.feedback = {
        error: null,
        result: null,
        busy: false,
        enableSshSupport: false,
        subject: '',
        type: 'app_error',
        description: '',
        appId: '',
        altEmail: ''
    };

    $scope.sshSupportEnabled = false;
    $scope.subscription = null;

    function resetFeedback() {
        $scope.feedback.enableSshSupport = false;
        $scope.feedback.subject = '';
        $scope.feedback.description = '';
        $scope.feedback.type = 'app_error';
        $scope.feedback.appId = '';
        $scope.feedback.altEmail = '';

        $scope.feedbackForm.$setUntouched();
        $scope.feedbackForm.$setPristine();
    }

    $scope.submitFeedback = function () {
        $scope.feedback.busy = true;
        $scope.feedback.result = null;
        $scope.feedback.error = null;

        var data = {
            enableSshSupport: $scope.feedback.enableSshSupport,
            subject: $scope.feedback.subject,
            description: $scope.feedback.description,
            type: $scope.feedback.type,
            appId: $scope.feedback.appId,
            altEmail: $scope.feedback.altEmail
        };

        Client.createTicket(data, function (error, result) {
            if (error) {
                $scope.feedback.error = error.message;
            } else {
                $scope.feedback.result = result;
                resetFeedback();
            }

            $scope.feedback.busy = false;

            // refresh state
            Client.getRemoteSupport(function (error, enabled) {
                if (error) return console.error(error);

                $scope.sshSupportEnabled = enabled;
            });
        });
    };

    $scope.toggleSshSupport = function () {
        Client.enableRemoteSupport(!$scope.sshSupportEnabled, function (error) {
            if (error) return $scope.error(error);

            $scope.sshSupportEnabled = !$scope.sshSupportEnabled;
        });
    };

    function getSubscription() {
        Client.getSubscription(function (error, result) {
            if (error && error.statusCode === 412) return; // not yet registered
            if (error) return console.error(error);

            $scope.subscription = result;
        });
    }

    Client.onReady(function () {
        getSubscription();

        Client.getSupportConfig(function (error, supportConfig) {
            if (error) return console.error(error);

            $scope.supportConfig = supportConfig;

            Client.getRemoteSupport(function (error, enabled) {
                if (error) return console.error(error);

                $scope.sshSupportEnabled = enabled;
            });
        });
    });

    $('.modal-backdrop').remove();
}]);
