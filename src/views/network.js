'use strict';

/* global angular */
/* global $ */

angular.module('Application').controller('NetworkController', ['$scope', '$location', 'Client', function ($scope, $location, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastAdmin) $location.path('/'); });

    $scope.user = Client.getUserInfo();
    $scope.config = Client.getConfig();

    // keep in sync with sysinfo.js
    $scope.sysinfoProvider = [
        { name: 'Public IP', value: 'generic' },
        { name: 'Static IP Address', value: 'fixed' },
        { name: 'Network Interface', value: 'network-interface' }
    ];

    $scope.prettySysinfoProviderName = function (provider) {
        switch (provider) {
        case 'generic': return 'Public IP';
        case 'fixed': return 'Static IP Address';
        case 'network-interface': return 'Network Interface';
        default: return 'Unknown';
        }
    };

    $scope.dyndnsConfigure = {
        busy: false,
        success: false,
        error: '',
        currentState: false,
        enabled: false,

        refresh: function () {
            Client.getDynamicDnsConfig(function (error, enabled) {
                if (error) return console.error(error);

                $scope.dyndnsConfigure.currentState = enabled;
                $scope.dyndnsConfigure.enabled = enabled;
            });
        },

        submit: function () {
            $scope.dyndnsConfigure.busy = true;
            $scope.dyndnsConfigure.success = false;
            $scope.dyndnsConfigure.error = '';

            Client.setDynamicDnsConfig($scope.dyndnsConfigure.enabled, function (error) {
                if (error) $scope.dyndnsConfigure.error = error.message;
                else $scope.dyndnsConfigure.currentState = $scope.dyndnsConfigure.enabled;

                $scope.dyndnsConfigure.busy = false;
                $scope.dyndnsConfigure.success = true;
            });
        }
    };

    $scope.blocklist  = {
        busy: false,
        error: {},
        blocklist: '',
        currentBlocklist: '',
        currentBlocklistLength: 0,

        refresh: function () {
            Client.getBlocklist(function (error, result) {
                if (error) return console.error(error);

                $scope.blocklist.currentBlocklist = result;
                $scope.blocklist.currentBlocklistLength = result.split('\n').filter(function (l) { return l.length !== 0 && l[0] !== '#'; }).length;
            });
        },

        show: function () {
            $scope.blocklist.error = {};
            $scope.blocklist.blocklist = $scope.blocklist.currentBlocklist;

            $('#blocklistModal').modal('show');
        },

        submit: function () {
            $scope.blocklist.error = {};
            $scope.blocklist.busy = true;

            Client.setBlocklist($scope.blocklist.blocklist, function (error) {
                $scope.blocklist.busy = false;
                if (error) {
                    $scope.blocklist.error.blocklist = error.message;
                    $scope.blocklist.error.ip = error.message;
                    $scope.blocklistChangeForm.$setPristine();
                    $scope.blocklistChangeForm.$setUntouched();
                    return;
                }

                $scope.blocklist.refresh();

                $('#blocklistModal').modal('hide');
            });
        }
    },

    $scope.sysinfo = {
        busy: false,
        error: {},

        serverIp: '',

        provider: '',
        ip: '',
        ifname: '',

        // configure dialog
        newProvider: '',
        newIp: '',
        newIfname: '',

        refresh: function () {
            Client.getSysinfoConfig(function (error, result) {
                if (error) return console.error(error);

                $scope.sysinfo.provider = result.provider;
                $scope.sysinfo.ip = result.ip || '';
                $scope.sysinfo.ifname = result.ifname || '';

                Client.getServerIp(function (error, ip) {
                    if (error) return console.error(error);

                    $scope.sysinfo.serverIp = ip;
                });
            });
        },

        show: function () {
            $scope.sysinfo.error = {};
            $scope.sysinfo.newProvider = $scope.sysinfo.provider;
            $scope.sysinfo.newIp = $scope.sysinfo.ip;
            $scope.sysinfo.newIfname = $scope.sysinfo.ifname;

            $('#sysinfoModal').modal('show');
        },

        submit: function () {
            $scope.sysinfo.error = {};
            $scope.sysinfo.busy = true;

            var config = {
                provider: $scope.sysinfo.newProvider
            };

            if (config.provider === 'fixed') {
                config.ip = $scope.sysinfo.newIp;
            } else if (config.provider === 'network-interface') {
                config.ifname = $scope.sysinfo.newIfname;
            }

            Client.setSysinfoConfig(config, function (error) {
                $scope.sysinfo.busy = false;
                if (error && error.message.indexOf('ip') !== -1) {
                    $scope.sysinfo.error.ip = error.message;
                    $scope.sysinfoForm.$setPristine();
                    $scope.sysinfoForm.$setUntouched();
                    return;
                } else if (error && (error.message.indexOf('interface') !== -1 || error.message.indexOf('IPv4') !== -1)) {
                    $scope.sysinfo.error.ifname = error.message;
                    $scope.sysinfoForm.$setPristine();
                    $scope.sysinfoForm.$setUntouched();
                    return;
                } else if (error) {
                    console.error(error);
                    return;
                }

                $scope.sysinfo.refresh();

                $('#sysinfoModal').modal('hide');
            });
        }
    };

    Client.onReady(function () {
        $scope.sysinfo.refresh();

        $scope.dyndnsConfigure.refresh();
        if ($scope.user.role === 'owner') $scope.blocklist.refresh();
    });

    $('.modal-backdrop').remove();
}]);
